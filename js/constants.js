export const constants = {
  defaults: {
    title: '#FCDB1960'
  },
  colors: {
    brand1: '#1681d1',
    brand2: '#003366',
    gray: '#bdbdbd'
  },
  imgs: {
    logo: require('./../img/logo.png'),
    menuIcon: require('./../img/toolbar_menu.png')
  },
  youtube: {
    resultsInRequest: 10,
    apiKey: 'AIzaSyDANt44l50Z5sEu-b5HHoEyCOG2_wCLbuU',//my
    channelId: 'UCuHmHyqMQWgC0ySkhkbErUQ'//dynamo-brest
  },
  localized: {
    drawer: {
      ru: {
        newsText: 'Новости',
        calendarText: 'Расписание матчей',
        tableText: 'Турнирная таблица',
        academyText: 'Детская академия',
        calendarLink: 'http://dynamo-brest.by/calendar',
        tableLink: 'http://dynamo-brest.by/calendar/table',
        academyLink: 'http://dynamo-brest.by/academy',
        eSportsLink: 'http://dynamo-brest.by/e-sports'
      },
      en: {
        newsText: 'News',
        calendarText: 'Calendar',
        tableText: 'Table',
        academyText: 'Youth academy',
        calendarLink: 'http://en.dynamo-brest.by/calendar',
        tableLink: 'http://en.dynamo-brest.by/calendar/table',
        academyLink: 'http://en.dynamo-brest.by/academy',
        eSportsLink: 'http://en.dynamo-brest.by/e-sports'
      }
    },
    lists: {
      ru: {
        locale: 'ru',
        moreText: 'Показать больше',
        requestLink: {
          broadcast: 'https://gist.githubusercontent.com/IvanKrasilnikov/349a3907b810f6b45f7c6faf6f3a261a/raw/26fb9fd3c92f19d0a28b77ef5863e38d409af68e/broadcast_ru.json',
          news: 'https://gist.githubusercontent.com/IvanKrasilnikov/0b3de69f6b8a4de001ea14441e78befd/raw/07b4ae4fb08c01831057436292bd8d5c4d404400/news_ru.json'
        }
      },
      en: {
        locale: 'en',
        moreText: 'Show more',
        requestLink: {
          broadcast: 'https://gist.githubusercontent.com/IvanKrasilnikov/b7f4cf4779e606f41a0cec91f6de0bcc/raw/7659de964a10c083b559868b479dc0c84c2cdf9a/broadcast_en.json',
          news: 'https://gist.githubusercontent.com/IvanKrasilnikov/41189cc8b8254041d749c93c42a3e5fc/raw/1bc879217dc6013e96db0c5fb3309898c2a8eed4/news_en.json'
        }
      }
    }
  }
}
